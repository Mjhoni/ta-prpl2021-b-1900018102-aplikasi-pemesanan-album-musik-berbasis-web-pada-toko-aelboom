<!doctype html>
  <html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="css/style.css">
    <title>aelboom</title>
  </head>
<section style="background-color: #222222;">
<img  src="foto/logo.png" alt="Gambar Logo" style="width:48px;height:48px;margin-top: 50px;margin-left: 20px;">
    <header style="margin-bottom:50px;">
        <center>
            <h1 >aelboom</h1>
            <p style="color:#ffffff">Website Pemesanan Album Terbaik Di Planet Pulto</p>
        </center>
        
    </header>
</section>
<nav>
    <li class="nav-item">
        <a class="nav-link active" aria-current="page" href="login.php">Login</a>
    </li>
</nav>
<?php
session_start();

$conn = mysqli_connect('localhost', 'root', '', 'db_aelboom');
if (!$conn) {
die("Koneksi gagal. " . mysqli_connect_error()); // close koneksi
} ?>

<div class="container mt-5">
	<h4>Laporan Transaksi</h4>
	<br>

	<table class="table table-bordered mt-3">
		<thead align="center">
			<tr>
				<th>#</th>
				<th>Tgl. Transaksi</th>
				<th>Total Item</th>
				<th>Total Bayar</th>
				<th>Aksi</th>
			</tr>
		</thead>
		<tbody align="center">

			<?php
			$query = mysqli_query($conn, "SELECT * FROM tb_order");
			$no = 1;
			while ($dt = $query->fetch_assoc()) :
				?>

				<tr>
					<td><?= $no++; ?></td>
					<td><?= $dt['tgl_transaksi']; ?></td>
					<td><?= $dt['total_item']; ?></td>
					<td><?= $dt['total_bayar']; ?></td>
					<td>
						<a href="detail_order.php?id_order=<?= $dt['id_order']; ?>">Detail Order</a>
					</td>
				</tr>

			<?php endwhile; ?>

		</tbody>
	</table>
</div>

<a href="index.php"><button class="btn btn-danger">Kembali ke beranda</button></a>
</div>