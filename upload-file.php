<?php
include 'koneksi.php';
$limit = 10 * 1024 * 1024;
$ekstensi =  array('png','jpg','jpeg','gif');
$jumlahFile = count($_FILES['foto']['name']);

for($x=0; $x<$jumlahFile; $x++){
	$namafile = $_FILES['foto']['name'][$x];
	$tmp = $_FILES['foto']['tmp_name'][$x];
	$tipe_file = pathinfo($namafile, PATHINFO_EXTENSION);
	$ukuran = $_FILES['foto']['size'][$x];	
	if($ukuran > $limit){
		header("location:form-upload-file.php?alert=gagal_ukuran");		
	}else{
		if(!in_array($tipe_file, $ekstensi)){
			header("location:form-upload-file.php?alert=gagal_ektensi");			
		}else{	
			//menyimpan data foto ke folder foto	
			move_uploaded_file($tmp, 'foto/'.date('d-m-Y').'-'.$namafile);
			$x = date('d-m-Y').'-'.$namafile;
			//memasukkan data gambar kedalam database
			mysqli_query($conn,"INSERT INTO tb_produk(gambar_nama) VALUES('$x')");
			header("location:form-upload-file.php?alert=simpan");
		}
	}
}