<!doctype html>
  <html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="css/style.css">
    <title>aelboom</title>
  </head>
<section style="background-color: #222222;">
<img  src="foto/logo.png" alt="Gambar Logo" style="width:48px;height:48px;margin-top: 50px;margin-left: 20px;">
    <header style="margin-bottom:50px;">
        <center>
            <h1 >aelboom</h1>
            <p style="color:#ffffff">Website Pemesanan Album Terbaik Di Planet Pulto</p>
        </center>
        
    </header>
</section>
<nav>
    <li class="nav-item">
        <a class="nav-link active" aria-current="page" href="login.php">Login</a>
    </li>
</nav>
<?php
session_start();

$conn = mysqli_connect('localhost', 'root', '', 'db_aelboom');
if (!$conn) {
die("Koneksi gagal. " . mysqli_connect_error()); // close koneksi
}

if (!isset($_GET['cari'])) {
$query = mysqli_query($conn, "SELECT * FROM tb_produk");
} else {
$query = mysqli_query($conn, "SELECT * FROM tb_produk WHERE nama_produk LIKE '%" . $_GET['cari'] . "%'");
}

if (isset($_SESSION['pesan'])) {
  echo '<div class="alert alert-warning alert-dismissible fade show" role="alert">
              ' . $_SESSION['pesan'] . '
              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">×</span>
              </button>
            </div>';

  unset($_SESSION['pesan']);
}
?>
<div class="container mt-5">

<?php require_once 'cart.php'; ?> 

<div class="row mb-2">
  <div class="col">
    <h4>Daftar Produk</h4>
  </div>
  <div class="col">
    <form class="form-inline pull-right" action="" method="GET">
      <div class="form-group mx-sm-3 mb-2">
        <input type="text" name="cari" class="form-control" placeholder="Cari Produk">
      </div>
      <button type="submit" class="btn btn-success mb-2">Cari</button>
    </form>
  </div>
</div>

<table class="table">
  <thead class="thead-light">
    <tr>
    <th scope="col">Album</th>
      <th scope="col">Nama Album</th>
      <th scope="col">Deskripsi</th>
      <th scope="col">Harga</th>
      <th scope="col">Stok</th>
      <th scope="col">Pembelian</th>
      <th scope="col">Aksi</th>
    </tr>
  </thead>
  <tbody>

  <?php
		if (isset($_GET['cari'])) {
			$cari = $_GET['cari'];
			echo "<b>&nbsp Hasil Pencarian : " . $cari . "</b>";
		}
		?>

    <?php
    $no = 1;
    if (isset($_GET['cari'])) {
			$cari = $_GET['cari'];
			$result = mysqli_query($conn, "SELECT * FROM tb_produk WHERE nama_barang LIKE '%" . $cari . "%'");
		} else {
			$result = mysqli_query($conn, "SELECT * FROM tb_produk");
		}
    while ($dt = $result->fetch_assoc()) :
    ?>

      <form method="POST" action="<?= $_SERVER['PHP_SELF']; ?>">
        <input type="hidden" name="id_produk" value="<?= $dt['id']; ?>"></input>
        <tr>
        <td><?php echo "<img src='foto/$dt[gambar_nama]' width='300' height='190' />";?></td>
         <td><?= $dt['nama_barang']; ?></td>
         <td><?= $dt['deskripsi']; ?></td>
          <td><?= $dt['harga']; ?></td>
          <td><?= $dt['stok']; ?></td>
          <td width="106">
            <input class="form-control form-control-sm" type="number" name="pembelian" value="1" min="1" max="<?= $dt['stok']; ?>"></td>
          <td>
            <button class="btn btn-primary btn-sm" type="submit" name="submit">
              <i class="fa fa-shopping-cart"></i>
            </button>
          </td>
        </tr>
      </form>

    <?php endwhile; ?>

  </tbody>
</table>

<a href="laporan.php"><button class="btn btn-danger">Lihat Laporan</button></a>
</div>