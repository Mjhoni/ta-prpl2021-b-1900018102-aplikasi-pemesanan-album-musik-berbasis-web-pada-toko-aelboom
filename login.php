<?php
include 'koneksi.php'; //menghubungkan ke database
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Form Login Admin aelboom</title>
    <link rel="stylesheet" type="text/css" href="css/bootstrap.css">
</head>
<body style=" background-color: #122b40;">
<a href="index.php">
	<button class="btn btn-success btn-sm"> Kembali Belanja</button>
</a>
<div class="container" style="margin-top:100px;" >
	<div class="row">
		<div class="col-md-12">
			<div class="col-md-3"></div>
			<div class="col-md-6">
				<div class="panel panel-warning">
					<div class="panel-heading">
						<div style="margin-top: 5px;margin-bottom: 100px;"></div>
							<div style="margin-left: 90px; margin-top: -90px; font-size: 120%;">
								A P L I K A S I &nbsp; PENJUALAN &nbsp; A L B U M
							</div>
							<div style="margin-left: 150px; font-size: 200%; margin-top: 10px;">
								<strong>Silahkan Login</strong>
							</div>
						</div>
                        <!-- isi -->
						<div class="panel-body">
							<div class="col-md-12">
								<form method="post" action='ceklogin.php'>
									<div class="form-group">
										<label>Username</label>
										<input type="text" name="username" class="form-control" placeholder="Masukan username Anda..." required maxlength="30">
									</div>
									<div class="form-group">
										<label>Password</label>
										<input type="password" name="password" class="form-control" placeholder="Masukan password Anda..." required maxlength="30">
									</div>

									<div class="form-group">
										<input type="submit" name="login" class="btn btn-warning btn-block btn-lg" value="LOGIN">
									</div>
								</form>

								</form>
							</div>
						</div>
						<!-- end isi -->
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
</body>
</html>